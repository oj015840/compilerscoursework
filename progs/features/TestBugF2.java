class TestBugF2 {
    public static void main(String[] a) {
		if (new TestF2().f2()) {
			System.out.println(0);
		} else {
			System.out.println(1);
		}
    }
}

class TestF2 {
	
	boolean yVisited;
	
    public boolean f2() {
	boolean x;
	yVisited = false;
	x = true;
	x = (x || this.y());
	return !yVisited;
	}
    
    public boolean y() {
	yVisited = true;
	return true;
    }

}

